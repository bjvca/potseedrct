#!/usr/bin/env python
import pygmaps
import csv
import webbrowser

### locate csv file
import Tkinter
from tkFileDialog import askopenfilename


file_path = askopenfilename()

cr = csv.reader(open(file_path,"rb" ))
mymap = pygmaps.maps(-1.15, 29.8,11)

for row in cr:  
	if row[2] == "Sel":
		### selection is green
    		mymap.addpoint(float(row[0]),  float(row[1]), "#00FF00","test")
	elif row[2] == "Store":
		### store is blue
		mymap.addpoint(float(row[0]),  float(row[1]), "#0000FF","test")
	elif row[2] == "Sel+Store":
		### crossed treatment is red
		mymap.addpoint(float(row[0]),  float(row[1]), "#FF0000","test")
	else:
		### control is black
		mymap.addpoint(float(row[0]),  float(row[1]), "#000000","test")


mymap.draw('mymap.draw.html') 
url = 'mymap.draw.html'
webbrowser.open_new(url)

