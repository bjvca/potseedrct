

library(foreign)
set.seed(12345)
source("/home/bjvca/data/projects/PASIC/potseedRCT/sampling/pairwise_rnd.R")
### get gps data
sec0 <- read.dta("/home/bjvca/data/projects/PASIC/data/potatoes/cleaned/PSEC0.dta")
### merge in hhsize using hhroster
sec1 <- read.dta("/home/bjvca/data/projects/PASIC/data/potatoes/cleaned/PSEC1.dta")
hhsize <- aggregate(sec1$id,list(sec1$hhid), FUN=max)
names(hhsize) <- c("hhid","hhsize")
dta <- merge(sec0,hhsize, by="hhid")

# add sexhead and agehead from hhroster
pothead <- subset(sec1,d5=="Head")
#keep d1 (sex) and d3 (age)
pothead <- pothead[,c("hhid","d1","d3")]
names(pothead) <- c("hhid","sexhead","agehead")
dta <- merge(dta,pothead, by="hhid")

### merge in productivity (prod) and area
prod <- read.dta("/home/bjvca/data/projects/PASIC/productivity/data/merged/productivity.dta")
prod <- subset(prod, crop=="pot")
prod <- subset(prod, season=="sec13")
area <-  aggregate(c(prod$area),list(prod$hhid), FUN=sum)
prod <-  aggregate(c(prod$prod),list(prod$hhid), FUN=mean)
names(prod) <- c("hhid","prod")
names(area) <- c("hhid","area_pot")
dta <- merge(dta,prod, by="hhid")
dta <- merge(dta,area, by="hhid")


# welfare
## for potatoes
sec11 <- read.dta("/home/bjvca/data/projects/PASIC/data/potatoes/cleaned/PSEC11.dta")
sec11$ex8 <- sec11$ex2 * sec11$ex6
sec11$ex8[is.na(sec11$ex8) ] <- 0
sec11$ex7[is.na(sec11$ex7) ] <- 0

sec11$all <- sec11$ex7+sec11$ex8

sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]<=22] <- sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]<=22]/7
sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]>22 & as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]<=46] <- sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]>22 & as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]<=46]/30
sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]>46] <- sec11$all[as.numeric(sec11$ex0)[!is.na(as.numeric(sec11$ex0))]>46
]/365
hhwelfare_pot <- aggregate(sec11$all,by=list(sec11$hhid), FUN=sum)
names(hhwelfare_pot) <- c("hhid","cons")
dta <- merge(dta, hhwelfare_pot, by="hhid")
dta$welfare_pc <- dta$cons/dta$hhsize

# names(df)[names(df) == 'old.var.name'] <- 'new.var.name'
names(dta)[names(dta) == "y2" ] <- "dist_input"

## credit
sec10 <- read.dta("/home/bjvca/data/projects/PASIC/data/potatoes/cleaned/PSEC10.dta")
sec10 <- aggregate(sec10$c2,by=list(sec10$hhid), FUN=sum)
names(sec10) <- c("hhid","credit")
dta <- merge(dta,sec10, by="hhid", all.x=T)
dta$credit[is.na(dta$credit)] <- 0

## extension on potatoes
dta$extension <- as.numeric(dta$hh16a=="Yes")

## this is new: the original file had only deg and min for latitude and longitude. we merge in sec from an older file
sec0old <- read.dta("/home/bjvca/data/projects/PASIC/data/potatoes/raw/version2/PSEC0.dta")
sec0old$latitude <- sec0old$gpsndeg + sec0old$gpsnmin/60 + sec0old$gpsnsec/3600
sec0old$longitude <- sec0old$gpsedeg + sec0old$gpsemin/60 + sec0old$gpsesec/3600
sec0old <- sec0old[c("hhid","latitude","longitude")]
dta <- merge(dta,sec0old,by="hhid")

## we realize that we should only focus on farmers that recycle seed here, as seed selection and storage are likely to be less relevant to farmers that buy seed from the market or get it as a gift

sec4a <- read.dta("/home/bjvca/data/projects/PASIC/productivity/data/pot/PSEC4A.dta")
sec4b <- read.dta("/home/bjvca/data/projects/PASIC/productivity/data/pot/PSEC4B.dta")
sec4arec <- subset(sec4a,sec4a$f7=="Own recycled seed" | sec4a$f7==" Own garden or farm" |sec4a$f7=="own farm")
sec4brec <- subset(sec4b,sec4b$s7=="Own recycled seed" | sec4b$s7==" Own garden or farm" |sec4b$s7=="own farm")
sec4arec <- sec4arec[,1]
sec4brec <- sec4brec[,1]
sec4rec <- c(sec4arec,sec4brec)
sec4rec <- sec4rec[!duplicated(sec4rec)]
dta <- dta[dta$hhid %in% sec4rec,]
dta_int <- dta
write.csv(dta,"/home/bjvca/data/projects/PASIC/potseedRCT/sampling/dta.csv")

##take first observations

dta <- dta[complete.cases(dta$latitude),]
dta <- subset(dta, latitude>0.5 & latitude <1.75)
dta <- dta[complete.cases(dta$longitude),]
dta <- subset(dta, longitude>29.5)
dta <- dta[complete.cases(dta$hhsize),]
dta <- subset(dta, hhsize<13)
dta <- dta[complete.cases(dta$prod),]
dta <- subset(dta, prod<20000 & prod > 200 )
dta <- dta[complete.cases(dta$agehead),]
dta$sexhead <- as.numeric(dta$sexhead=="Female")
dta <- dta[complete.cases(dta$area_pot),]
dta <- subset(dta, area_pot<5)
dta <- subset(dta, welfare_pc<10000)
dta <- dta[complete.cases(dta$credit),]
dta <- dta[complete.cases(dta$welfare_pc),]
dta <- dta[complete.cases(dta$dist_input),]
dta <- dta[complete.cases(dta$extension),]
dta$prod <- log(dta$prod)
dta$welfare_pc  <- log(dta$welfare_pc)

pairwise_rnd(dta, c("hhsize", "agehead","prod","sexhead", "area_pot","welfare_pc", "dist_input","credit","extension"), c("latitude", "longitude"))

res <- data.frame(res)
names(res) <- c("hhid","treat","bloc")
merged <- merge(dta,res,by="hhid")

merged <- merged[,c("hhid","treat","bloc")]
merged_opt <- merged[order(as.numeric(as.character(merged$bloc))),]
### these are optimal, they include max distance criterion and are seed recyclers

### second best, seed recyclers that have missing coordinates data (which we replace by random data)
dta <- dta_int
dta <- dta[!complete.cases(dta$latitude),]
dta$latitude <- runif(length(dta$latitude),-1,1)
dta$longitude <- runif(length(dta$longitude),-1,1)

dta <- dta[complete.cases(dta$hhsize),]
dta <- subset(dta, hhsize<13)
dta <- dta[complete.cases(dta$prod),]
dta <- subset(dta, prod<20000 & prod > 200 )
dta <- dta[complete.cases(dta$agehead),]
dta$sexhead <- as.numeric(dta$sexhead=="Female")
dta <- dta[complete.cases(dta$area_pot),]
dta <- subset(dta, area_pot<5)
dta <- subset(dta, welfare_pc<10000)
dta <- dta[complete.cases(dta$credit),]
dta <- dta[complete.cases(dta$welfare_pc),]
dta <- dta[complete.cases(dta$dist_input),]
dta <- dta[complete.cases(dta$extension),]
dta$prod <- log(dta$prod)
dta$welfare_pc  <- log(dta$welfare_pc)

pairwise_rnd(dta, c("hhsize", "agehead","prod","sexhead", "area_pot","welfare_pc", "dist_input","credit","extension"), c("latitude", "longitude"))

res <- data.frame(res)
names(res) <- c("hhid","treat","bloc")
merged <- merge(dta,res,by="hhid")

merged <- merged[,c("hhid","treat","bloc")]
merged_sec <- merged[order(as.numeric(as.character(merged$bloc))),]

merged_sec$bloc <- as.numeric(as.character(merged_sec$bloc)) + 59
merged_opt$bloc <- as.numeric(as.character(merged_opt$bloc))
merged_all <- rbind(merged_opt, merged_sec)



write.csv(merged_all,"/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_pot_rnd.csv")


dta <- dta_int
mergefin <- merge(merged_all,dta, by="hhid")
## balancing tests
## compare each treatment to control 
summary(lm(hhsize~treat=="Sel",data=mergefin))
summary(lm(hhsize~treat=="Store",data=mergefin))
summary(lm(hhsize~treat=="Sel+Store",data=mergefin))

summary(lm(agehead~treat=="Sel",data=mergefin))
summary(lm(agehead~treat=="Store",data=mergefin))
summary(lm(agehead~treat=="Sel+Store",data=mergefin))

summary(lm(prod~treat=="Sel",data=mergefin))
summary(lm(prod~treat=="Store",data=mergefin))
summary(lm(prod~treat=="Sel+Store",data=mergefin))




### joint orthogonality
summary(lm(treat=="Sel"~hhsize+agehead+sexhead+prod+area_pot+welfare_pc+dist_input+credit+extension, data=mergefin))
summary(lm(treat=="Store"~hhsize+agehead+sexhead+prod+area_pot+welfare_pc+dist_input+credit+extension, data=mergefin))
summary(lm(treat=="Sel+Store"~hhsize+agehead+sexhead+prod+area_pot+welfare_pc+dist_input+credit+extension, data=mergefin))



### for testing purpose only, household that received potato farmers as gift 
nodta <- dta_int
sec4a <- read.dta("/home/bjvca/data/projects/PASIC/productivity/data/pot/PSEC4A.dta")
sec4arec <- subset(sec4a,sec4a$f7=="Fellow farmer" & !(sec4a$f7=="Own recycled seed" & sec4a$f7==" Own garden or farm" & sec4a$f7=="own farm"))
sec4arec <- sec4arec[,1]
sec4arec <- sec4arec[!duplicated(sec4arec)]
nodta <- nodta[nodta$hhid %in% sec4arec,]

##take first observations

nodta <- nodta[complete.cases(nodta$latitude),]
nodta <- subset(nodta, latitude>0.5 & latitude <1.75)
nodta <- nodta[complete.cases(nodta$longitude),]
nodta <- subset(nodta, longitude>29.5)
nodta <- nodta[complete.cases(nodta$hhsize),]
nodta <- subset(nodta, hhsize<13)
nodta <- nodta[complete.cases(nodta$prod),]
nodta <- subset(nodta, prod<20000 & prod > 200 )
nodta <- nodta[complete.cases(nodta$agehead),]
nodta$sexhead <- as.numeric(nodta$sexhead=="Female")
nodta <- nodta[complete.cases(nodta$area_pot),]
nodta <- subset(nodta, area_pot<5)
nodta <- subset(nodta, welfare_pc<10000)
nodta <- nodta[complete.cases(nodta$credit),]
nodta <- nodta[complete.cases(nodta$welfare_pc),]
nodta <- nodta[complete.cases(nodta$dist_input),]
nodta <- nodta[complete.cases(nodta$extension),]

pairwise_rnd(nodta, c("hhsize", "agehead","prod","sexhead", "area_pot","welfare_pc", "dist_input","credit","extension"), c("latitude", "longitude"))

res <- data.frame(res)
names(res) <- c("hhid","treat","bloc")
nomerged <- merge(nodta,res,by="hhid")

nomerged <- nomerged[,c("hhid","treat","bloc")]
nomerged <- nomerged[order(as.numeric(as.character(nomerged$bloc))),]
write.csv(nomerged,"/home/bjvca/data/projects/PASIC/potseedRCT/sampling/trainer_pot_rnd.csv")


sample <- read.csv("/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_pot_rnd.csv")
trainer <- read.csv("/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_pot_rnd.csv")

duplicated(rbind(sample,trainer)$hhid)
