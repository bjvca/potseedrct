sample <- read.csv("/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_ID.csv")
treated <- subset(sample,sample$treat != "Ctrl")

### correcting village (ea) names
treated$ea <- as.character(treated$ea)

treated$ea[treated$ea == "BUGARA A"] <- "BUGARA"
treated$ea[treated$ea == "RWABARERA"] <- "RWABAREERA"
treated$ea[treated$ea == "BWAYO"] <- "BWAYU"
treated$ea[treated$ea == "GAHEMBE A"] <- "GAHEMBE"
treated$ea[treated$ea == "KATAGIRAMAIZI A"] <- "KATAGIRAMAIZI"
treated$ea[treated$ea == "KATAGIRAMIZI A"] <- "KATAGIRAMAIZI"
treated$ea[treated$ea == "KATAGIRAMIZI"] <- "KATAGIRAMAIZI"
treated$ea[treated$ea == "KIBURARA A"] <- "KIBURARA"
treated$ea[treated$ea == "KIDAKAMA B"] <- "KIDAKAMA"
treated$ea[treated$ea == "KANYABITARA LCI A"] <- "KANYABITARA"
treated$ea[treated$ea == "KYAJURA B"] <- "KYAJURA"
treated$ea[treated$ea == "KYANJURA B"] <- "KYAJURA"
treated$ea[treated$ea == "KYAZURA"] <- "KYAJURA"
treated$ea[treated$ea == "KYAZURA"] <- "KYAJURA"
treated$ea[treated$ea == "OMURUSHASHA B"] <- "OMURUSHASHA"
treated$ea[treated$ea == "MURUSHASHA"] <- "OMURUSHASHA"
treated$ea[treated$ea == "RUSHASHA"] <- "OMURUSHASHA"
treated$ea[treated$ea == "RUHAGO A"] <- "RUHAGO"
treated$ea[treated$ea == "BUTENGO C"] <- "BUTENGO"
treated$ea[treated$ea == "MULORA"] <- "MURORA"
treated$ea[treated$ea == "MURORA B"] <- "MURORA"
treated$ea[treated$ea == "RUGESHI A"] <- "RUGESHI"
treated$ea[treated$ea == "KIGATA A"] <- "KIGATA"
treated$ea[treated$ea == "RUGESHI A"] <- "RUGESHI"
treated$ea[treated$ea == "BUTUGA B"] <- "BUTUGA"

treated$ea <- factor(treated$ea)

tab <- table(treated$district,treated$ea)
### villages in Kabale
print(rownames(tab)[1])
colnames(tab)[tab[1,] >0]
### villages in kanungu
print(rownames(tab)[2])
colnames(tab)[tab[2,] >0]
### villages in kisoro
print(rownames(tab)[3])
colnames(tab)[tab[3,] >0]

tab2 <- table(treated$ea,treated$hhid)
for (i in 1:dim(tab2)[1]) {
print(rownames(tab2)[i])
print(colnames(tab2)[tab2[i,] >0])
}

control <- subset(sample,sample$treat == "Ctrl")

### correcting village (ea) names
control$ea <- as.character(control$ea)

control$ea[control$ea == "BUGARA A"] <- "BUGARA"
control$ea[control$ea == "RWABARERA"] <- "RWABAREERA"
control$ea[control$ea == "BWAYO"] <- "BWAYU"
control$ea[control$ea == "GAHEMBE A"] <- "GAHEMBE"
control$ea[control$ea == "KATAGIRAMAIZI A"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KATAGIRAMIZI A"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KATAGIRAMIZI"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KIBURARA A"] <- "KIBURARA"
control$ea[control$ea == "KIDAKAMA B"] <- "KIDAKAMA"
control$ea[control$ea == "KANYABITARA LCI A"] <- "KANYABITARA"
control$ea[control$ea == "KYAJURA B"] <- "KYAJURA"
control$ea[control$ea == "KYANJURA B"] <- "KYAJURA"
control$ea[control$ea == "KYAZURA"] <- "KYAJURA"
control$ea[control$ea == "KYAZURA"] <- "KYAJURA"
control$ea[control$ea == "OMURUSHASHA B"] <- "OMURUSHASHA"
control$ea[control$ea == "MURUSHASHA"] <- "OMURUSHASHA"
control$ea[control$ea == "RUSHASHA"] <- "OMURUSHASHA"
control$ea[control$ea == "RUHAGO A"] <- "RUHAGO"
control$ea[control$ea == "BUTENGO C"] <- "BUTENGO"
control$ea[control$ea == "MULORA"] <- "MURORA"
control$ea[control$ea == "MURORA B"] <- "MURORA"
control$ea[control$ea == "RUGESHI A"] <- "RUGESHI"
control$ea[control$ea == "KIGATA A"] <- "KIGATA"
control$ea[control$ea == "RUGESHI A"] <- "RUGESHI"
control$ea[control$ea == "BUTUGA B"] <- "BUTUGA"
control$ea[control$ea == "MURUSHASHA B"] <- "MURUSHASHA"
control$ea[control$ea == "GIKORO B"] <- "GIKORO"

control$ea <- factor(control$ea)

tab <- table(control$district,control$ea)
### villages in Kabale
print(rownames(tab)[1])
colnames(tab)[tab[1,] >0]
### villages in kanungu
print(rownames(tab)[2])
colnames(tab)[tab[2,] >0]
### villages in kisoro
print(rownames(tab)[3])
colnames(tab)[tab[3,] >0]

tab2 <- table(control$ea,control$hhid)
for (i in 1:dim(tab2)[1]) {
print(rownames(tab2)[i])
print(colnames(tab2)[tab2[i,] >0])
}
