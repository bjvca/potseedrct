clear
clear matrix
cap log close
set more off


* Where to find the data
*gl data	= "Y:\potseedrct\data\"
gl data = "C:\Users\u0105058\Desktop\potseedrct_local\data\"

use "$data/endlineV1.dta", clear

***********************
** Attrition balance **
***********************

keep hh_id treatsel treatstore plant_potato
destring hh_id, replace
rename hh_id hhid

gen attrition=1 if plant_potato==0
replace attrition=0 if attrition==.


tab treatsel
tab treatstore

** Balancing table with attrition

* File 1

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC0.dta"
keep if _m==3
drop _m

* Experience
gen farm_experience=2014-hh2
egen exp_median=median(farm_experience)
replace farm_experience=exp_median if farm_experience==.

reg farm_experience treatsel treatstore

* Fertilizer use
recode hh12 (2=0)
reg hh12 treatsel treatstore

* Received training
recode hh15 (2=0)
reg hh15 treatsel treatstore

* Received credit
recode hh43a (2=0)
replace hh43a=0 if hh43a==.
reg hh43a treatsel treatstore


* Closest input provider
reg y2 treatsel treatstore

* Nearest market
egen y6_median=median(y6)
replace y6=y6_median if y6==.
reg y6 treatsel treatstore


* File 2

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC1.dta"
keep if _m==3
drop _m

bys hhid: egen hh_size=count(id)
keep if d5==1

* Household size
reg hh_size treatsel treatstore

* Age
reg d3 treatsel treatstore

* Male head
recode d1 (2=0)
reg d1 treatsel treatstore

* Read/write
recode d8 (2=0) (3=0)
replace d8=0 if d8==.
reg d8 treatsel treatstore

* File 3

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC2.dta"
keep if _m==3
drop _m

* Time to reach parcel
replace ll3=0 if ll3==.
reg ll3 treatsel treatstore


duplicates drop hhid, force

* File 4
merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC7a.dta"
keep if _m==3
drop _m

*  % sold

gen sold_ware=(m8a+m5a)/m2
replace sold_ware=0 if sold_ware==.
reg sold_ware treatsel treatstore

* % saved as seed

gen saved_seed=m4/m2
replace saved_seed=0 if saved_seed==.
reg saved_seed treatsel treatstore

* Log potato production

gen potato_prod=log(m2)
reg potato_prod treatsel treatstore
reg m2 treatsel treatstore

duplicates drop hhid, force

* Consumption

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC11.dta"
keep if _m==3
drop _m

gen value=ex2*ex6
bys hhid: egen total_value=total(value)
bys hhid: egen cons=total(ex7)
gen consumption=total_value+cons

duplicates drop hhid, force
gen log_cons=log(consumption)

reg log_cons treatsel treatstore

* Potato share

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta"
keep if _m==3
drop _m

reg potato_share treatsel treatstore
replace potato_share=0 if potato_share==.



* Joint orthogonality

reg treatsel log_cons m2 saved_seed sold_ware ll3 d8 d1 d3 hh_size y6 y2 hh43a hh15 farm_experience hh12

reg attrition i.treatsel##c.potato_share i.treatsel##c.log_cons i.treatsel##c.m2 i.treatsel##c.saved_seed i.treatsel##c.sold_ware i.treatsel##c.ll3 i.treatsel##i.d8 i.treatsel##i.d1 ///
	i.treatsel##c.d3 i.treatsel##c.hh_size i.treatsel##c.y6 i.treatsel##c.y2 i.treatsel##i.hh43a i.treatsel##i.hh15 i.treatsel##c.farm_experience i.treatsel##i.hh12
testparm i.treatsel i.treatsel#c.potato_share i.treatsel#c.log_cons i.treatsel#c.m2 i.treatsel#c.saved_seed i.treatsel#c.sold_ware i.treatsel#c.ll3 i.treatsel#i.d8 i.treatsel#i.d1 ///
	i.treatsel#c.d3 i.treatsel#c.hh_size i.treatsel#c.y6 i.treatsel#c.y2 i.treatsel#i.hh43a i.treatsel#i.hh15 i.treatsel#c.farm_experience i.treatsel#i.hh12

reg attrition i.treatstore##c.potato_share i.treatstore##c.log_cons i.treatstore##c.m2 i.treatstore##c.saved_seed i.treatstore##c.sold_ware i.treatstore##c.ll3 i.treatstore##i.d8 i.treatstore##i.d1 ///
	i.treatstore##c.d3 i.treatstore##c.hh_size i.treatstore##c.y6 i.treatstore##c.y2 i.treatstore##i.hh43a i.treatstore##i.hh15 i.treatstore##c.farm_experience i.treatstore##i.hh12
testparm i.treatstore i.treatstore#c.potato_share i.treatstore#c.log_cons i.treatstore#c.m2 i.treatstore#c.saved_seed i.treatstore#c.sold_ware i.treatstore#c.ll3 i.treatstore#i.d8 i.treatstore#i.d1 ///
	i.treatstore#c.d3 i.treatstore#c.hh_size i.treatstore#c.y6 i.treatstore#c.y2 i.treatstore#i.hh43a i.treatstore#i.hh15 i.treatstore#c.farm_experience i.treatstore#i.hh12

	
**********************************
** Spillover conditions balance **	
**********************************

use "$data/endlineV1_gps.dta", clear

	
keep hh_id cond_sel cond_store
destring hh_id, replace
rename hh_id hhid

* File 1

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC0.dta"
keep if _m==3
drop _m
	
* Experience
gen farm_experience=2014-hh2
egen exp_median=median(farm_experience)
replace farm_experience=exp_median if farm_experience==.
reg farm_experience i.cond_sel
reg farm_experience i.cond_store

* Fertilizer use
recode hh12 (2=0)
reg hh12 i.cond_sel
reg hh12 i.cond_store

* Received training
recode hh15 (2=0)
reg hh15 i.cond_sel
reg hh15 i.cond_store

* Received credit
recode hh43a (2=0)
reg hh43a i.cond_sel
reg hh43a i.cond_store

* Closest input provider
reg y2 i.cond_sel
reg y2 i.cond_store

* Nearest market
egen y6_median=median(y6)
replace y6=y6_median if y6==.
reg y6 i.cond_sel
reg y6 i.cond_store
	
	
* File 2

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC1.dta"
keep if _m==3
drop _m

bys hhid: egen hh_size=count(id)
keep if d5==1

* Household size
reg hh_size i.cond_sel
reg hh_size i.cond_store

* Age
reg d3 i.cond_sel
reg d3 i.cond_store

* Male head
recode d1 (2=0)
reg d1 i.cond_sel
reg d1 i.cond_store

* Read/write
recode d8 (2=0) (3=0)
replace d8=0 if d8==.
reg d8 i.cond_sel
reg d8 i.cond_store

* File 3

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC2.dta"
keep if _m==3
drop _m

* Time to reach parcel
replace ll3=0 if ll3==.
reg ll3 i.cond_sel
reg ll3 i.cond_store

duplicates drop hhid, force

* File 4
merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC7a.dta"
keep if _m==3
drop _m

*  % sold

gen sold_ware=(m8a+m5a)/m2
replace sold_ware=0 if sold_ware==.
reg sold_ware i.cond_sel
reg sold_ware i.cond_store

* % saved as seed

gen saved_seed=m4/m2
replace saved_seed=0 if saved_seed==.
reg saved_seed i.cond_sel
reg saved_seed i.cond_store

* Log potato production

gen potato_prod=log(m2)
reg potato_prod i.cond_sel
reg potato_prod i.cond_store
reg m2 i.cond_sel
reg m2 i.cond_store

duplicates drop hhid, force

* Consumption

merge 1:m hhid using "C:\Users\u0105058\Desktop\pot\pot\PSEC11.dta"
keep if _m==3
drop _m

gen value=ex2*ex6
bys hhid: egen total_value=total(value)
bys hhid: egen cons=total(ex7)
gen consumption=total_value+cons

duplicates drop hhid, force
gen log_cons=log(consumption)

reg log_cons i.cond_sel
reg log_cons i.cond_store

* Potato share

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta"
keep if _m==3
drop _m
replace potato_share=0 if potato_share==.

reg potato_share i.cond_sel
reg potato_share i.cond_store

	
	
	
	
