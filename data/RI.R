### this R script implements Randomization Inference for the potseedRCT
### see for example http://blogs.worldbank.org/impactevaluations/tools-of-the-trade-estimating-correct-standard-errors-in-small-sample-cluster-studies-another-take

RI <- function(treat, out, nrep) {
set.seed(12345)

library(combinat)
#dta <- subset(dta, bloc!=2 & bloc!=5)
ntreat <- "Sel"
if (treat == "Sel") {
ntreat <- "Store"
}


res <- array(NA,c(62,24))
### create all possible combinations for the selection treatment
### this creates a matrix with all possible assignments in each block (64 blocks x 24 (4!) assignments)

for (i in 1:62) {
res[i,] <- colSums(1*(matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == "Sel+Store" | matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == treat) * out[dta$bloc==i])  - colSums(1*(matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == ntreat | matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == "Ctrl") * out[dta$bloc==i]) 
}


### in theory, one should estimate all permutations, but I could not figure out how to do this except by writing 62 nested loops
### and this would take ages to estimate, so I just randomly draw from all possible permutations

dist  <- array(NA,nrep)
for (repl in 1:nrep) {
subtot <- 0
for (i in 1:62) {
subtot <- subtot + res[i,sample(1:24, 1)]
}
dist[repl] <- subtot
}

print(sum(abs(dist/122) >= mean(out[dta$video_shown==treat | dta$video_shown=="Sel+Store"]) -  mean(out[dta$video_shown==ntreat | dta$video_shown=="Ctrl"]))/nrep)
}


RIreg <- function(treat, out, nrep) {
set.seed(12345)
library(combinat)
dist  <- array(NA,nrep)
for (repl in 1:nrep) {

res <- array(NA,c(62,24))
tot <- array(NA,c(1,3))
for (i in 1:62) {
tot <- rbind(tot, cbind(1*(matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == "Sel+Store" | matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) ==treat)[,sample(1:24,1)] , out[dta$bloc==i],i) )
}

tot <- data.frame(tot)
names(tot) <- c("treat","resp","bloc")
dist[repl] <- coef(lm(resp~treat+as.factor(bloc),data=tot))["treat"]
}
dta$ind <- dta$video_shown=="Store" | dta$video_shown=="Sel+Store"
if (treat=="Sel") {
dta$ind <- dta$video_shown=="Sel" | dta$video_shown=="Sel+Store"
}
print(coef(lm(out~ ind +as.factor(bloc),data=dta))["indTRUE"])
print(sum(abs(dist) >= coef(lm(out~ ind +as.factor(bloc),data=dta))["indTRUE"])/nrep)
}

RIreg2 <- function(treat, out,interact, nrep) {
set.seed(12345)
library(combinat)
dist  <- array(NA,nrep)
for (repl in 1:nrep) {

res <- array(NA,c(62,24))
tot <- array(NA,c(1,4))
for (i in 1:62) {
tot <- rbind(tot, cbind(1*(matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == "Sel+Store" | matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) ==treat)[,sample(1:24,1)] , out[dta$bloc==i],interact[dta$bloc==i],i) )
}

tot <- data.frame(tot)
names(tot) <- c("treat","resp","interact","bloc")
dist[repl] <- coef(lm(resp~treat*interact +as.factor(bloc),data=tot))["treat"]
}
dta$ind <- dta$video_shown=="Store" | dta$video_shown=="Sel+Store"
if (treat=="Sel") {
dta$ind <- dta$video_shown=="Sel" | dta$video_shown=="Sel+Store"
}
print(coef(lm(out~ ind*interact +as.factor(bloc),data=dta))["indTRUE"])
print(sum(abs(dist) >= coef(lm(out~ ind*interact +as.factor(bloc),data=dta))["indTRUE"])/nrep)
}

RIreg3 <- function(treat, out, nrep) {
set.seed(12345)
library(combinat)
dist  <- matrix(NA,nrep,5)
for (repl in 1:nrep) {

res <- array(NA,c(62,24))
tot <- array(NA,c(1,5))
for (i in 1:62) {
tot <- rbind(tot, cbind(1*(matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) == "Sel+Store" | matrix(unlist(permn(c("Ctrl","Sel","Store","Sel+Store"))),c(4,24)) ==treat)[,sample(1:24,1)] , out[dta$bloc==i],dta$knowledgePSS[dta$bloc==i],dta$knowledgePSSH[dta$bloc==i],i) )
}

tot <- data.frame(tot)
names(tot) <- c("treat","resp","knowledgePSS","knowledgePSSH","bloc")
temp <- coef(lm(resp~treat*knowledgePSS + treat*knowledgePSSH  +as.factor(bloc),data=tot))
dist[repl,] <- temp[c(2:4, length(temp)-1,length(temp))]
}
dta$ind <- dta$video_shown=="Store" | dta$video_shown=="Sel+Store"
if (treat=="Sel") {
dta$ind <- dta$video_shown=="Sel" | dta$video_shown=="Sel+Store"
}
st1 <- coef(lm(out~ ind*knowledgePSS + ind*knowledgePSSH  +as.factor(bloc),data=dta))
print(st1[c(2,3,4,length(st1)-1,length(st1))])
print(sum(abs(dist[,1]) >= st1[2])/nrep)
print(sum(abs(dist[,2]) >= st1[3])/nrep)
print(sum(abs(dist[,3]) >= st1[4])/nrep)
print(sum(abs(dist[,4]) >= st1[length(st1)-1])/nrep)
print(sum(abs(dist[,5]) >= st1[length(st1)])/nrep)
}
