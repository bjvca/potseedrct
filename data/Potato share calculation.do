clear
clear matrix
cap log close
set more off



use "C:\Users\u0105058\Desktop\pot\pot\PSEC7A.dta"

foreach var in m5b m6b m8b m9b {
	replace `var'=0 if `var'==.
}

bys hhid: egen potato_sales_A=total(m5b+ m6b+ m8b+ m9b)

duplicates drop hhid, force

keep hhid potato_sales_A

save "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta", replace

use "C:\Users\u0105058\Desktop\pot\pot\PSEC7B.dta"

foreach var in n5b n6b n8b n9b {
	replace `var'=0 if `var'==.
}

bys hhid: egen potato_sales_B=total(n5b +n6b +n8b +n9b )

duplicates drop hhid, force

keep hhid potato_sales_B

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta"
drop _m
save "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta", replace

use "C:\Users\u0105058\Desktop\pot\pot\PSEC8A.dta", clear

foreach var in h19 h22 h23 h26 {
	replace `var'=0 if `var'==.
}

bys hhid: egen crop_sales_A=total((h19*h22)+(h23*h26))

duplicates drop hhid, force

keep hhid crop_sales_A

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta"
drop _m
save "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta", replace


use "C:\Users\u0105058\Desktop\pot\pot\PSEC8B.dta", clear

foreach var in k19 k22 k23 k26 {
	replace `var'=0 if `var'==.
}

bys hhid: egen crop_sales_B=total((k19*k22)+(k23*k26))

duplicates drop hhid, force

keep hhid crop_sales_B

merge 1:1 hhid using "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta"
drop _m
save "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta", replace

foreach var in crop_sales_B crop_sales_A potato_sales_B potato_sales_A {
	replace `var'=0 if `var'==.
}

gen potato_share=(potato_sales_A+potato_sales_B)/(crop_sales_A+crop_sales_B+potato_sales_A+potato_sales_B)

sum potato_share
save "C:\Users\u0105058\Desktop\pot\pot\merge_file.dta", replace



