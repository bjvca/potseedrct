clear
clear matrix
cap log close
set more off


* Where to find the data
*gl data	= "Y:\potseedrct\data\"
gl data = "C:\Users\u0105058\Desktop\potseedrct_local\data\"

log using "$data/Endline_analysis.txt", replace

{
************************
** Knowledge analysis **
************************

use "$data/endlineV1.dta", clear

gen easter=1 if date=="2017-04-19" | date=="2017-04-20" | date=="2017-04-21" | date=="2017-04-22"
replace easter=0 if easter==.

* Total knowledge score=number of correct answers (max=9)

egen know_score=rowtotal(sel1_bin sel2_bin sel3_bin store1_bin store2_bin store3_bin gen1_bin gen2_bin gen3_bin)
ttest know_score, by(treatsel)
ttest know_score, by(treatstore) // No significant differences in total scores


** PSS treatment

foreach x in sel1 sel2 sel3 {

	quietly cibar `x'_bin, over1(treatsel) graphopts(ytitle("Correct (0/1)") xtitle("`x'") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(`x', replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
}
	
graph combine sel1 sel2 sel3, rows(1) cols(3) title("Knowledge effect of PSS treatment")

* Regressions with block fixed effects (only coefficients for treatment are shown)

matrix PSS=J(2,10,.)
local i=1

foreach x in sel1_bin sel2_bin sel3_bin store1_bin store2_bin store3_bin gen1_bin gen2_bin gen3_bin know_score {

	ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc
	mat B=r(b)
	local value=B[1,1]
	di `value'
	mat PSS[1,`i']=`value'
	mat P=r(p)
	local pi=P[1,1]
	mat PSS[2,`i']=`pi'
	local ++i
}

putexcel A1=matrix(PSS) using Knowledge, sheet("PSS") replace

** PSSH treatment

foreach x in store1 store2 store3 {

	quietly cibar `x'_bin, over1(treatstore) graphopts(ytitle("Correct (0/1)") xtitle("`x'") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.1 "Not seen PSSH" 2.8 "Seen PSSH") name(`x', replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
}
	
graph combine store1 store2 store3, rows(1) cols(3) title("Knowledge effect of PSSH treatment")

* Regressions with block fixed effects (only coefficients for treatment are shown)

matrix PSSH=J(2,10,.)
local i=1

foreach x in sel1_bin sel2_bin sel3_bin store1_bin store2_bin store3_bin gen1_bin gen2_bin gen3_bin know_score {

	ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc
	mat B=r(b)
	local value=B[1,1]
	di `value'
	mat PSSH[1,`i']=`value'
	mat P=r(p)
	local pi=P[1,1]
	mat PSSH[2,`i']=`pi'
	local ++i
}

putexcel A1=matrix(PSSH) using Knowledge, sheet("PSSH") modify

* For means

foreach x in sel1_bin sel2_bin sel3_bin store1_bin store2_bin store3_bin gen1_bin gen2_bin gen3_bin know_score {
	reg `x' treatsel treatstore
}

* Knowledge index as dependent variable

// Seed selection

foreach x in sel1_bin sel2_bin sel3_bin {
	egen std_`x'=sd(`x') if treatsel==0
	sort treatsel
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'.
	drop std_`x' mean_`x'
}

corrmat dm_sel1_bin dm_sel2_bin dm_sel3_bin, covmat(mat_sel) // Generate variance covariance matrix for outcomes
matrix mat_seli=inv(mat_sel) // Inverted matrix
gen weight_sel1=mat_seli[1,1]+mat_seli[1,2]+mat_seli[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_sel2=mat_seli[2,1]+mat_seli[2,2]+mat_seli[2,3]
gen weight_sel3=mat_seli[3,1]+mat_seli[3,2]+mat_seli[3,3]

gen know_sel= weight_sel1*dm_sel1_bin+weight_sel2*dm_sel2_bin+weight_sel3*dm_sel3_bin
drop dm_* weight_*

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg know_sel treatsel treatstore i.bloc // Coefficient: 0.3658383; p-value: 0.1046

// Shorter command:
gen std_group_sel=1 if treatsel==0
gen wgt=1
local local_know_sel sel1_bin sel2_bin sel3_bin
make_index_gr know_sel wgt std_group_sel `local_know_sel'
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg index_know_sel treatsel treatstore i.bloc // Coefficient: 0.2137677; p-value: 0.1042


// Seed storage

foreach x in store1_bin store2_bin store3_bin {
	egen std_`x'=sd(`x') if treatstore==0
	sort treatstore
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_store1_bin dm_store2_bin dm_store3_bin, covmat(mat_store) // Generate variance covariance matrix for outcomes
matrix mat_storei=inv(mat_store) // Inverted matrix
gen weight_store1=mat_storei[1,1]+mat_storei[1,2]+mat_storei[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_store2=mat_storei[2,1]+mat_storei[2,2]+mat_storei[2,3]
gen weight_store3=mat_storei[3,1]+mat_storei[3,2]+mat_storei[3,3]

gen know_store= weight_store1*dm_store1_bin+weight_store2*dm_store2_bin+weight_store3*dm_store3_bin
drop dm_* weight_*

ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg know_store treatsel treatstore i.bloc // Coefficient: 0.0332795; p-value: 0.8783

// Shorter command:
gen std_group_store=1 if treatstore==0
local local_know_store store1_bin store2_bin store3_bin
make_index_gr know_store wgt std_group_store `local_know_store'
ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg index_know_store treatsel treatstore i.bloc // Coefficient: 0.0205622; p-value: 0.8796

************
** Inputs **
************

** Seed (probability of buying)

gen seed_purchased=1 if seed_acquisition==1
replace seed_purchased=0 if seed_acquisition!=1 & seed_acquisition!=.

quietly cibar seed_purchased, over1(treatsel) graphopts(ytitle("Prob of buying seed") xtitle("PSS") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar seed_purchased, over1(treatstore) graphopts(ytitle("Prob of buying seed") xtitle("PSSH") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Probability of buying seed")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg seed_purchased treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg seed_purchased treatsel treatstore i.bloc

** Seed (quantity used)

destring plot_select_area, replace
gen seed_per_acre=seed_quantity/plot_select_area

quietly cibar seed_per_acre, over1(treatsel) graphopts(ytitle("Seed used (in kgs/acre)") xtitle("PSS") xsize(3) ylab(0(200)800) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar seed_per_acre, over1(treatstore) graphopts(ytitle("Seed used (in kgs/acre)") xtitle("PSSH") xsize(3) ylab(0(200)800) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Seed used")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg seed_per_acre treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg seed_per_acre treatsel treatstore i.bloc 

** Fertilizer (used)

quietly cibar fertilizer, over1(treatsel) graphopts(ytitle("Prob of using fertilizer") xtitle("PSS") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar fertilizer, over1(treatstore) graphopts(ytitle("Prob of using fertilizer") xtitle("PSSH") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Probability of using fertilizer")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg fertilizer treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg fertilizer treatsel treatstore i.bloc 

** Chemicals (used)

quietly cibar chemicals, over1(treatsel) graphopts(ytitle("Prob of using chemicals") xtitle("PSS") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar chemicals, over1(treatstore) graphopts(ytitle("Prob of using chemicals") xtitle("PSSH") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Probability of using chemicals")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg chemicals treatsel treatstore i.bloc
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg chemicals treatsel treatstore i.bloc

** Labor hired

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg labor_hired treatsel treatstore i.bloc
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg labor_hired treatsel treatstore i.bloc

* For means

foreach x in seed_purchased seed_per_acre fertilizer chemicals labor_hired {
	reg `x' treatsel treatstore
}

* Input index as dependent variable

// Seed selection

foreach x in fertilizer chemicals labor_hired {
	egen std_`x'=sd(`x') if treatsel==0
	sort treatsel
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_fertilizer dm_chemicals dm_labor_hired, covmat(mat_sel) // Generate variance covariance matrix for outcomes
matrix mat_seli=inv(mat_sel) // Inverted matrix
gen weight_sel1=mat_seli[1,1]+mat_seli[1,2]+mat_seli[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_sel2=mat_seli[2,1]+mat_seli[2,2]+mat_seli[2,3]
gen weight_sel3=mat_seli[3,1]+mat_seli[3,2]+mat_seli[3,3]

gen input_sel= weight_sel1*dm_fertilizer+weight_sel2*dm_chemicals+weight_sel3*dm_labor_hired
drop dm_* weight_*

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg input_sel treatsel treatstore i.bloc // Coefficient: 0.0571272 ; p-value: 0.7767

// Shorter command:
local local_input_sel fertilizer chemicals labor_hired
make_index_gr input_sel wgt std_group_sel `local_input_sel'
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg index_input_sel treatsel treatstore i.bloc // Coefficient: 0.0404009; p-value: 0.7684

// Seed storage

foreach x in fertilizer chemicals labor_hired {
	egen std_`x'=sd(`x') if treatstore==0
	sort treatstore
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_fertilizer dm_chemicals dm_labor_hired, covmat(mat_store) // Generate variance covariance matrix for outcomes
matrix mat_storei=inv(mat_store) // Inverted matrix
gen weight_store1=mat_storei[1,1]+mat_storei[1,2]+mat_storei[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_store2=mat_storei[2,1]+mat_storei[2,2]+mat_storei[2,3]
gen weight_store3=mat_storei[3,1]+mat_storei[3,2]+mat_storei[3,3]

gen input_store= weight_store1*dm_fertilizer+weight_store2*dm_chemicals+weight_store3*dm_labor_hired
drop dm_* weight_*

ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg input_store treatsel treatstore i.bloc // Coefficient: 0.0792535; p-value: 0.6871

// Shorter command:
local local_input_store fertilizer chemicals labor_hired
make_index_gr input_store wgt std_group_store `local_input_store'
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg index_input_store treatsel treatstore i.bloc // Coefficient: 0.0554436; p-value: 0.6880

***************
** Practices **
***************

** PSS treatment

quietly cibar practices_seed_num, over1(treatsel) graphopts(ytitle("Number of practices applied") xtitle("Seed selection unaided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS_unaided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_seed_aid_num, over1(treatsel) graphopts(ytitle("Number of practices applied") xtitle("Seed selection aided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS_aided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_store_num, over1(treatsel) graphopts(ytitle("Number of practices applied") xtitle("Seed storage unaided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSSH_unaided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_store_aid_num, over1(treatsel) graphopts(ytitle("Number of practices applied") xtitle("Seed storage aided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSSH_aided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS_unaided PSS_aided PSSH_unaided PSSH_aided, rows(2) cols(2) title("Applied practices after PSS treatment")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg practices_seed_num treatsel treatstore i.bloc 
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg practices_seed_aid_num treatsel treatstore i.bloc 
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg practices_store_num treatsel treatstore i.bloc 
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg practices_store_aid_num treatsel treatstore i.bloc 

** PSSH treatment

quietly cibar practices_seed_num, over1(treatstore) graphopts(ytitle("Number of practices applied") xtitle("Seed selection unaided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSS_unaided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_seed_aid_num, over1(treatstore) graphopts(ytitle("Number of practices applied") xtitle("Seed selection aided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSS_aided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_store_num, over1(treatstore) graphopts(ytitle("Number of practices applied") xtitle("Seed storage unaided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH_unaided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_store_aid_num, over1(treatstore) graphopts(ytitle("Number of practices applied") xtitle("Seed storage aided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH_aided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS_unaided PSS_aided PSSH_unaided PSSH_aided, rows(2) cols(2) title("Applied practices after PSSH treatment")

ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg practices_seed_num treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg practices_seed_aid_num treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg practices_store_num treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg practices_store_aid_num treatsel treatstore i.bloc 

* For means

foreach x in practices_seed_num practices_seed_aid_num practices_store_num practices_store_aid_num {
	reg `x' treatsel treatstore 
}


**************************
** Practices separately **
**************************

foreach x in practices_seed_aid1 practices_seed_aid2 practices_seed_aid3 practices_seed_aid4 practices_seed_aid5 practices_seed_aid6 {
	destring `x', replace
	ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc 
}

foreach x in practices_seed_aid1 practices_seed_aid2 practices_seed_aid3 practices_seed_aid4 practices_seed_aid5 practices_seed_aid6 {
	ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc 
}

foreach x in practices_seed_aid1 practices_seed_aid2 practices_seed_aid3 practices_seed_aid4 practices_seed_aid5 practices_seed_aid6 {
	reg `x' treatsel treatstore
}


foreach x in practices_store_aid1 practices_store_aid2 practices_store_aid3 practices_store_aid4 practices_store_aid5 practices_store_aid6 {
	destring `x', replace
	ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc 
}

foreach x in practices_store_aid1 practices_store_aid2 practices_store_aid3 practices_store_aid4 practices_store_aid5 practices_store_aid6 {
	ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg `x' treatsel treatstore i.bloc 
}

foreach x in practices_store_aid1 practices_store_aid2 practices_store_aid3 practices_store_aid4 practices_store_aid5 practices_store_aid6 {
	reg `x' treatsel treatstore
}

* Practices index as dependent variable

// Seed selection

foreach x in practices_seed_aid1 practices_seed_aid2 practices_seed_aid3 practices_seed_aid4 practices_seed_aid5 practices_seed_aid6 {
	destring `x', replace
	egen std_`x'=sd(`x') if treatsel==0
	sort treatsel
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_*, covmat(mat_sel) // Generate variance covariance matrix for outcomes
matrix mat_seli=inv(mat_sel) // Inverted matrix
gen weight_sel1=mat_seli[1,1]+mat_seli[1,2]+mat_seli[1,3]+mat_seli[1,4]+mat_seli[1,5]+mat_seli[1,6] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_sel2=mat_seli[2,1]+mat_seli[2,2]+mat_seli[2,3]+mat_seli[2,4]+mat_seli[2,5]+mat_seli[2,6] 
gen weight_sel3=mat_seli[3,1]+mat_seli[3,2]+mat_seli[3,3]+mat_seli[3,4]+mat_seli[3,5]+mat_seli[3,6] 
gen weight_sel4=mat_seli[4,1]+mat_seli[4,2]+mat_seli[4,3]+mat_seli[4,4]+mat_seli[4,5]+mat_seli[4,6] 
gen weight_sel5=mat_seli[5,1]+mat_seli[5,2]+mat_seli[5,3]+mat_seli[5,4]+mat_seli[5,5]+mat_seli[5,6] 
gen weight_sel6=mat_seli[6,1]+mat_seli[6,2]+mat_seli[6,3]+mat_seli[6,4]+mat_seli[6,5]+mat_seli[6,6]

gen practices_sel= weight_sel1*dm_practices_seed_aid1+weight_sel2*dm_practices_seed_aid2+weight_sel3*dm_practices_seed_aid3+weight_sel4*dm_practices_seed_aid4+weight_sel5*dm_practices_seed_aid5+weight_sel6*dm_practices_seed_aid6
drop dm_* weight_*

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg practices_sel treatsel treatstore i.bloc // Coefficient: 0.7206082; p-value: 0.0367

// Shorter command
local local_pract_sel practices_seed_aid1 practices_seed_aid2 practices_seed_aid3 practices_seed_aid4 practices_seed_aid5 practices_seed_aid6
make_index_gr pract_sel wgt std_group_sel `local_pract_sel'
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg index_pract_sel treatsel treatstore i.bloc // Coefficient: 0.2861739; p-value: 0.0521


// Seed storage

foreach x in practices_store_aid1 practices_store_aid2 practices_store_aid3 practices_store_aid4 practices_store_aid5 practices_store_aid6 {
	destring `x', replace
	egen std_`x'=sd(`x') if treatstore==0
	sort treatstore
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_*, covmat(mat_store) // Generate variance covariance matrix for outcomes
matrix mat_storei=inv(mat_store) // Inverted matrix
gen weight_store1=mat_storei[1,1]+mat_storei[1,2]+mat_storei[1,3]+mat_storei[1,4]+mat_storei[1,5]+mat_storei[1,6] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_store2=mat_storei[2,1]+mat_storei[2,2]+mat_storei[2,3]+mat_storei[2,4]+mat_storei[2,5]+mat_storei[2,6] 
gen weight_store3=mat_storei[3,1]+mat_storei[3,2]+mat_storei[3,3]+mat_storei[3,4]+mat_storei[3,5]+mat_storei[3,6] 
gen weight_store4=mat_storei[4,1]+mat_storei[4,2]+mat_storei[4,3]+mat_storei[4,4]+mat_storei[4,5]+mat_storei[4,6] 
gen weight_store5=mat_storei[5,1]+mat_storei[5,2]+mat_storei[5,3]+mat_storei[5,4]+mat_storei[5,5]+mat_storei[5,6] 
gen weight_store6=mat_storei[6,1]+mat_storei[6,2]+mat_storei[6,3]+mat_storei[6,4]+mat_storei[6,5]+mat_storei[6,6]

gen practices_store_index= weight_store1*dm_practices_store_aid1+weight_store2*dm_practices_store_aid2+weight_store3*dm_practices_store_aid3+weight_store4*dm_practices_store_aid4+weight_store5*dm_practices_store_aid5+weight_store6*dm_practices_store_aid6
drop dm_* weight_*

ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg practices_store_index treatsel treatstore i.bloc // Coefficient: 0.0358128; p-value: 0.9118

// Shorter command:
local local_pract_store practices_store_aid1 practices_store_aid2 practices_store_aid3 practices_store_aid4 practices_store_aid5 practices_store_aid6
make_index_gr pract_store wgt std_group_store `local_pract_store'
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg index_pract_store treatsel treatstore i.bloc // Coefficient: 0.0119566; p-value: 0.9371


***********
** Yield **
***********

quietly cibar harvest_yield if harvest_yield<100000, over1(treatsel) graphopts(ytitle("Yield (in kgs/acre)") xtitle("PSS") xsize(3) ylab(0(1000)5000) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar harvest_yield if harvest_yield<100000, over1(treatstore) graphopts(ytitle("Yield (in kgs/acre)") xtitle("PSSH") xsize(3) ylab(0(1000)5000) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Yield effect of treatments")

gen log_yield=ln(harvest_yield)

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg log_yield treatsel treatstore i.bloc if harvest_yield<100000
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg log_yield treatsel treatstore i.bloc if harvest_yield<100000


***********
** Sales **
***********

gen prop_sold=harvest_sold/harvest_calculated

quietly cibar prop_sold, over1(treatsel) graphopts(ytitle("Percentage sold") xtitle("PSS") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar prop_sold, over1(treatstore) graphopts(ytitle("Percentage sold") xtitle("PSSH") xsize(3) ylab(0(0.2)1) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Sales effect of treatments")

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg prop_sold treatsel treatstore i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg prop_sold treatsel treatstore i.bloc 


*****************
** Consumption **
*****************

destring hh_size, replace

quietly cibar consumption, over1(treatsel) graphopts(ytitle("Consumption value (in UGX)") xtitle("PSS") xsize(3) ylab(0(50000)150000) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSS, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
quietly cibar consumption, over1(treatstore) graphopts(ytitle("Consumption value (in UGX)") xtitle("PSSH") xsize(3) ylab(0(50000)150000) ///
	legend(off) xlab(1.3 "Not seen PSSH" 2.6 "Seen PSSH") name(PSSH, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))
	
graph combine PSS PSSH, rows(1) cols(2) title("Consumption effect of treatments")

gen log_consumption=ln(consumption)

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg log_consumption treatsel treatstore easter hh_size i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg log_consumption treatsel treatstore easter hh_size i.bloc 

* For means

reg log_yield treatsel treatstore if harvest_yield<100000
reg consumption treatsel treatstore easter hh_size if bloc_complete==1

foreach x in harvest_yield prop_sold log_consumption {
	reg `x' treatsel treatstore 
}

* Index for final outcomes

// Seed selection

foreach x in log_yield prop_sold log_consumption {
	egen std_`x'=sd(`x') if treatsel==0
	sort treatsel
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_log_yield dm_prop_sold dm_log_consumption, covmat(mat_sel) // Generate variance covariance matrix for outcomes
matrix mat_seli=inv(mat_sel) // Inverted matrix
gen weight_sel1=mat_seli[1,1]+mat_seli[1,2]+mat_seli[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_sel2=mat_seli[2,1]+mat_seli[2,2]+mat_seli[2,3]
gen weight_sel3=mat_seli[3,1]+mat_seli[3,2]+mat_seli[3,3]

gen final_sel= weight_sel1*dm_log_yield+weight_sel2*dm_prop_sold+weight_sel3*dm_log_consumption
drop dm_* weight_*

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg final_sel treatsel treatstore i.bloc // Coefficient: 0.1972791 ; p-value: 0.4063

// Shorter command
local local_final_sel log_yield prop_sold log_consumption
make_index_gr final_sel wgt std_group_sel `local_final_sel'
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg index_final_sel treatsel treatstore i.bloc // Coefficient: 0.1419583; p-value: 0.3285

// Seed storage

foreach x in log_yield prop_sold log_consumption {
	egen std_`x'=sd(`x') if treatstore==0
	sort treatstore
	replace std_`x'=std_`x'[1] if std_`x'==.
	egen mean_`x'=mean(`x')
	gen dm_`x'=(`x'-mean_`x')/std_`x'
	drop std_`x' mean_`x'
}

corrmat dm_log_yield dm_prop_sold dm_log_consumption, covmat(mat_store) // Generate variance covariance matrix for outcomes
matrix mat_storei=inv(mat_store) // Inverted matrix
gen weight_store1=mat_storei[1,1]+mat_storei[1,2]+mat_storei[1,3] // Weights are sums of row elements in inverted variance covariance matrix
gen weight_store2=mat_storei[2,1]+mat_storei[2,2]+mat_storei[2,3]
gen weight_store3=mat_storei[3,1]+mat_storei[3,2]+mat_storei[3,3]

gen final_store= weight_store1*dm_log_yield+weight_store2*dm_prop_sold+weight_store3*dm_log_consumption
drop dm_* weight_*

ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg final_store treatsel treatstore i.bloc // Coefficient: 0.2355062 ; p-value: 0.3843

// Shorter command:
local local_final_store log_yield prop_sold log_consumption
make_index_gr final_store wgt std_group_store `local_final_store'
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg index_final_store treatsel treatstore i.bloc // Coefficient: 0.125212; p-value: 0.3891

*********************
** Locus questions **
*********************

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus1 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus1 treatsel treatstore  i.bloc

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus2 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus2 treatsel treatstore  i.bloc 

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus3 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus3 treatsel treatstore  i.bloc 

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus4 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus4 treatsel treatstore  i.bloc

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus5 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus5 treatsel treatstore  i.bloc 

ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg locus6 treatsel treatstore  i.bloc 
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg locus6 treatsel treatstore  i.bloc 

** For means:

foreach x in locus1 locus2 locus3 locus4 locus5 locus6 {
	reg `x' treatsel treatstore
}

** Factor analysis

factor locus1 locus2 locus3 locus4 locus5 locus6
	screeplot // Eigenvalue: 1 ; Screeplot: 1 --> 1 factor retained
	factor locus1 locus2 locus3 locus4 locus5 locus6, factor(1)
	rotate, oblique quartimin
	predict motivation_factor
	label var motivation_factor "Motivation factor"
	
ritest treatsel _b[treatsel], reps(10000) strata(bloc) nodots: reg motivation_factor treatsel treatstore  i.bloc if bloc_complete==1
ritest treatstore _b[treatstore], reps(10000) strata(bloc) nodots: reg motivation_factor treatsel treatstore  i.bloc if bloc_complete==1

***************************************
** Spillover analysis - descriptives **
***************************************

use "$data/endline_spillovers.dta", clear

duplicates drop parent_index, force

sort parent_index
drop hh_id

merge 1:1 parent_index using "$data/endlineV1.dta"
drop know_fellow discuss_video know_fellow_treat discuss_video_treat
keep if _m==3
drop _m
drop if bloc==.

** Get a sense of spillovers

tab network
sum network

/*
   network |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |          2        0.83        0.83
          1 |          3        1.24        2.07
          2 |         13        5.39        7.47
          3 |         11        4.56       12.03
          4 |         28       11.62       23.65
          5 |         37       15.35       39.00
          6 |         31       12.86       51.87
          7 |         36       14.94       66.80
          8 |         26       10.79       77.59
          9 |         22        9.13       86.72
         10 |         19        7.88       94.61
         11 |          5        2.07       96.68
         12 |          2        0.83       97.51
         13 |          3        1.24       98.76
         14 |          2        0.83       99.59
         15 |          1        0.41      100.00
------------+-----------------------------------
      Total |        241      100.00

 Variable |       Obs        Mean    Std. Dev.       Min        Max
-------------+--------------------------------------------------------
     network |       241    6.448133    2.700001          0         15

--> 99% of farmers knows at least one other farmer in the sample (on average 6)	
	  
*/

tab network_video
sum network_video

/*

network_vid |
         eo |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |         98       40.66       40.66
          1 |         42       17.43       58.09
          2 |         22        9.13       67.22
          3 |         21        8.71       75.93
          4 |         14        5.81       81.74
          5 |         14        5.81       87.55
          6 |         10        4.15       91.70
          7 |          8        3.32       95.02
          8 |          5        2.07       97.10
          9 |          1        0.41       97.51
         10 |          3        1.24       98.76
         11 |          2        0.83       99.59
         12 |          1        0.41      100.00
------------+-----------------------------------
      Total |        241      100.00

 Variable |       Obs        Mean    Std. Dev.       Min        Max
-------------+--------------------------------------------------------
network_vi~o |       241    2.091286    2.644169          0         12

--> Almost 60% of farmers have talked to another farmer in the sample about the video (on average 2)

*/

tab network_video_treat
sum network_video_treat

/*

network_vid |
   eo_treat |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |        109       45.04       45.04
          1 |         43       17.77       62.81
          2 |         29       11.98       74.79
          3 |         21        8.68       83.47
          4 |         19        7.85       91.32
          5 |          9        3.72       95.04
          6 |          6        2.48       97.52
          7 |          2        0.83       98.35
          9 |          2        0.83       99.17
         10 |          2        0.83      100.00
------------+-----------------------------------
      Total |        242      100.00

    Variable |       Obs        Mean    Std. Dev.       Min        Max
-------------+--------------------------------------------------------
network_vi~t |       242    1.541322     2.01456          0         10

--> Almost 55% of farmers have talked to at least one treatment farmer about the video (on average 1.5)

*/

** Spillovers for 'pure' control farmers

tab network_treat if treat=="Ctrl"

/*

network_tre |
         at |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |          2        3.39        3.39
          1 |          2        3.39        6.78
          2 |          3        5.08       11.86
          3 |          9       15.25       27.12
          4 |          7       11.86       38.98
          5 |         16       27.12       66.10
          6 |          8       13.56       79.66
          7 |          8       13.56       93.22
          8 |          1        1.69       94.92
          9 |          2        3.39       98.31
         12 |          1        1.69      100.00
------------+-----------------------------------
      Total |         59      100.00

--> Almost all 'pure' control farmers know at least one treatment farmer in the village

*/

tab network_video_treat if treat=="Ctrl"

/*
	  
network_vid |
   eo_treat |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |         40       67.80       67.80
          1 |          9       15.25       83.05
          2 |          3        5.08       88.14
          3 |          2        3.39       91.53
          4 |          2        3.39       94.92
          5 |          3        5.08      100.00
------------+-----------------------------------
      Total |         59      100.00

--> Of the pure control farmers, 32% talked to at least one treatment farmer

*/

** Spillovers for control farmers in 2x2 design

tab network_video_treat if treatsel==0
sum network_video_treat if treatsel==0

/*

network_vid |
   eo_treat |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |         65       54.17       54.17
          1 |         26       21.67       75.83
          2 |          8        6.67       82.50
          3 |          8        6.67       89.17
          4 |          4        3.33       92.50
          5 |          4        3.33       95.83
          6 |          4        3.33       99.17
         10 |          1        0.83      100.00
------------+-----------------------------------
      Total |        120      100.00
	  
  Variable |       Obs        Mean    Std. Dev.       Min        Max
-------------+--------------------------------------------------------
network_vi~t |       120    1.133333    1.796043          0         10


--> 45% of control farmers for the PSS treatment have talked to at least one PSS treatment farmer

*/

tab network_video_treat if treatstore==0
sum network_video_treat if treatstore==0

/*

network_vid |
   eo_treat |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |         65       53.72       53.72
          1 |         16       13.22       66.94
          2 |         13       10.74       77.69
          3 |         11        9.09       86.78
          4 |          7        5.79       92.56
          5 |          6        4.96       97.52
          6 |          1        0.83       98.35
          9 |          1        0.83       99.17
         10 |          1        0.83      100.00
------------+-----------------------------------
      Total |        121      100.00

   Variable |       Obs        Mean    Std. Dev.       Min        Max
-------------+--------------------------------------------------------
network_vi~t |       121    1.305785    1.914171          0         10

--> Roughly the same applies to control farmers for the PSSH treatment

*/

tab network_video_control if treat!="Ctrl"

/* 

network_vid |
 eo_control |      Freq.     Percent        Cum.
------------+-----------------------------------
          0 |        123       67.21       67.21
          1 |         33       18.03       85.25
          2 |         13        7.10       92.35
          3 |         11        6.01       98.36
          4 |          2        1.09       99.45
          6 |          1        0.55      100.00
------------+-----------------------------------
      Total |        183      100.00

--> About 32% of treatment farmers talked to a control farmer 
--> Confirms findings from control farmers
--> Being talked to and talking to roughly give the same result
--> Some treatment farmers can be considered more talkative than others because more control farmers report being talked to than treatment farmers reporting to control farmers

*/

** Spillover heterogeneity

gen store_spill=1 if network_video_treat>1 & network_video_treat!=. & treatstore==0
replace store_spill=0 if network_video_treat<=1 & treatstore==0
gen sel_spill=1 if network_video_treat>1 & network_video_treat!=. & treatsel==0
replace sel_spill=0 if network_video_treat<=0 & treatsel==0

preserve

	replace treatstore=1 if store_spill==1 & treatstore==0
	replace treatsel=1 if sel_spil==1 & treatsel==0
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg harvest_yield treatsel treatstore i.bloc if harvest_yield<50000

restore


preserve 

	drop if treatstore==0 & network_video_treat>0
	tab treatstore
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg consumption treatsel treatstore  i.bloc if bloc_complete==1

restore

reg store2_bin network_treat i.bloc
tab network_treat if treatstore==0

* Generate some new variables



gen contaminated_PSSH=1 if network_video_treat>0 & treatstore==0

sort bloc contaminated_PSSH
by bloc: gen contaminated_bloc_PSSH=1 if contaminated_PSSH[1]==1
replace contaminated_bloc_PSSH=0 if contaminated_bloc_PSSH==.

reg store2_bin i.treatstore##i.contaminated_bloc_PSSH if bloc_complete==1

reg consumption i.treatstore##i.contaminated_bloc_PSSH i.bloc if bloc_complete==1
reg sel1_bin network_video_treat i.bloc if bloc_complete==1

reg consumption treatstore##c.network_video_treat i.bloc if bloc_complete==1

drop if network_video_treat>0 & treatstore==0

reg practices_store_aid_num spillover i.bloc if treatstore==0

gen spillover=1 if network_video_treat>0
replace spillover=0 if network_video_treat==0 & network_video_treat!=.

quietly cibar practices_store_aid_num if treatstore==0, over1(spillover) graphopts(ytitle("Number of practices applied") xtitle("Seed storage unaided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not contaminated" 2.6 "Contaminated") name(PSSH_unaided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

quietly cibar practices_store_aid_num, over1(treatsel) graphopts(ytitle("Number of practices applied") xtitle("Seed storage aided") xsize(3) ylab(0(1)4) ///
	legend(off) xlab(1.3 "Not seen PSS" 2.6 "Seen PSS") name(PSSH_aided, replace)) bargap(30) baropts(color(gs8)) ciopts(lcolor(gs3))

	
** Village intensity of treatment

bys village: egen village_num=count(parent_index)
bys village: egen village_treat_num=total(treat!="Ctrl")
gen treat_intensity=village_treat_num / village_num

** New way of analyzing: starting from the "being talked to" perspective

use "$data/spillover_analysis.dta", clear

duplicates drop hh_id, force

sort hh_id
drop parent_index
keep hh_id talked*

merge 1:1 hh_id using "$data/endlineV1.dta"
keep if _m==3
drop _m

** Treat contaminated farmers as treated

preserve

	replace treatsel=1 if talked_sel>1 & treatsel==0
	ritest treatsel _b[treatsel], reps(1000) strata(bloc) nodots: reg sel3_bin treatsel treatstore i.bloc
	ritest treatsel _b[treatsel], reps(1000) strata(bloc) nodots: reg locus1 treatsel treatstore i.bloc
	ritest treatsel _b[treatsel], reps(1000) strata(bloc) nodots: reg locus3 treatsel treatstore i.bloc
	ritest treatsel _b[treatsel], reps(1000) strata(bloc) nodots: reg locus4 treatsel treatstore i.bloc
	ritest treatsel _b[treatsel], reps(1000) strata(bloc) nodots: reg practices_seed_num treatsel treatstore i.bloc

restore

preserve

	replace treatstore=1 if talked_store>1 & treatstore==0
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg gen1_bin treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg gen2_bin treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg locus2 treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg locus3 treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg seed_purchased treatsel treatstore i.bloc

restore

preserve

	replace treatstore=1 if talked_store>1 & treatstore==0
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg gen1_bin treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg gen2_bin treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg locus2 treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg locus3 treatsel treatstore i.bloc
	ritest treatstore _b[treatstore], reps(1000) strata(bloc) nodots: reg seed_purchased treatsel treatstore i.bloc

restore

** Compare amongst control farmers

* For storage

preserve

	gen cont_store=1 if talked_store>1
	replace cont_store=0 if talked_store<=1 & treatstore==0
	tab cont_store
	reg locus1 cont_store i.bloc

restore

}

** New analysis

use "$data/endlineV1_gps.dta", clear 


reg index_know_sel i.cond_sel i.bloc
reg index_know_store i.cond_store i.bloc
reg index_pract_sel i.cond_sel i.bloc
reg index_pract_store i.cond_store i.bloc
reg index_input_sel i.cond_sel i.bloc
reg index_input_store i.cond_store i.bloc
reg index_final_sel i.cond_sel i.bloc
reg index_final_store i.cond_store i.bloc


	