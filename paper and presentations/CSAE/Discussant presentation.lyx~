#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article-beamer
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Farmer Beliefs and Counterfeiting
\end_layout

\begin_layout Subtitle
Evidence from Uganda
\end_layout

\begin_layout Author
Naureen Karachiwalla
\end_layout

\begin_layout Date
March 20, 2017
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
3 points
\end_layout

\end_inset


\end_layout

\begin_layout Enumerate
Results: the main results in this paper all come from those farmers that
 participated in the game.
 
\end_layout

\begin_deeper
\begin_layout Enumerate
However, balancing tables clearly show that these farmers are more educated,
 are younger and use more herbicide
\end_layout

\begin_layout Enumerate
It is to be expected that they are more informed about the extent of counterfeit
ing in the market
\end_layout

\begin_layout Enumerate
Hence, you might be overstating the extent to which farmers in general are
 informed
\end_layout

\begin_layout Enumerate
It would be useful to also have a robustness check with the Likert scale
 for all farmers as an outcome variable
\end_layout

\end_deeper
\begin_layout Enumerate
What about prices of the herbicide?
\end_layout

\begin_deeper
\begin_layout Enumerate
If farmers are informed consumers, even at the local level, should the prices
 not vary across markets?
\end_layout

\begin_layout Enumerate
Do you find evidence of lower prices in markets where difference between
 beliefs and actual counterfeiting is lower?
\end_layout

\begin_layout Enumerate
If not, the social cost of counterfeiting might be higher than claimed in
 the paper (see point 1)
\end_layout

\end_deeper
\begin_layout Enumerate

\end_layout

\end_body
\end_document
