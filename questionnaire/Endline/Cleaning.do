use "Desktop/test.dta", clear

foreach x of varlist sel1 sel2 sel3 sel4 store1 store2 store3 store4 gen1 gen2 gen3 gen4 {
	egen tot_`x'=rowtotal(`x' `x'_001 `x'_002 `x'_003)
}

foreach var of varlist *_2 {
 	local newname = substr("`var'", 1, length("`var'")-2)
  	rename `var' `newname'
}

foreach var of varlist * {
	label var `var' ""
}

foreach x of varlist note4-sold_times {
	egen tot_`x'=rowtotal(`x' `x'_2)
}
